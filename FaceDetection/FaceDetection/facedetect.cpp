//The MIT License(MIT)
//
//Copyright(c) 2014 Fabian Wahlster - razor@singul4rity.com
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions :
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

#include <Windows.h>

using namespace cv;
using namespace std;

inline bool create_dir(const std::string& szPath) {
    if (CreateDirectoryA(szPath.c_str(),NULL) != 0){
        return true;
    }else{
        return (GetLastError() == ERROR_ALREADY_EXISTS);
    }
}

int main(int argc, char *argv[])
{
	if(argc < 7){
		cout << "Usage: output/dir dataset.txt classifier.xml devID width height" << endl;
		return EXIT_FAILURE;
	}

	string output_dir = string(argv[1]);

	int devID = atoi(argv[4]);
	int width = atoi(argv[5]);
	int height = atoi(argv[6]);

	if(devID < 0 || width < 100 || height < 100){
		cout << "Arguments devID, width and height can't be negative/smaller 100!" << endl;
		return EXIT_FAILURE;
	}

	if(create_dir(output_dir) == false){
		cout << "Unable to create directory " << output_dir << endl;
		return EXIT_FAILURE;
	}

	fstream datasetlist(argv[2],ios::in | ios::app);
	if(datasetlist.is_open() == false){
		cout << "Unable to open datasetlist " << argv[1] << endl;
		return EXIT_FAILURE;
	}

    map<string,int> names;
	string name,counter;	
	while(datasetlist.good() && getline(datasetlist,name)){
		stringstream ss(name);
		getline(ss,name,';');getline(ss,name);//twice to get Name at the end
		if(name.empty()) continue;
		names[name]++;
	}

	datasetlist.clear();//reset stream for append

	CascadeClassifier haar_cascade;
	string cascade_file = string(argv[3]);
	haar_cascade.load(cascade_file);

	VideoCapture cam(devID);
	if(cam.isOpened() == false){
		cout << "Unable to open Device with ID " << devID << endl;
		datasetlist.close();
		return EXIT_FAILURE;
	}

	bool train = true;

	vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);

	cout << "Press N (new name) , SPACE (discard image), ENTER (use image)" << endl;

	string curName,tmp;Mat frame,face,resized_face,grayscale;vector< Rect_<int> > rects;
	while(train){
		cam >> frame;
		rects.clear();
		cvtColor(frame,grayscale,CV_BGR2GRAY);
		haar_cascade.detectMultiScale(grayscale,rects);
		cout << rects.size() << " Faces detected" << endl;
		for(unsigned int i = 0;i<rects.size() && train == true;++i){
			face = grayscale(rects[i]);
			resize(face, resized_face, Size(width, height), 1.0, 1.0, INTER_CUBIC);
			bool no_choice = true;
			while(no_choice){
				imshow("Detected Face",resized_face);
				int key = waitKey(0);
				cv::destroyWindow("Detected Face");
				if(curName.empty() && key != VK_SPACE) key = (int)'N';
				switch(key){
				case VK_ESCAPE: //close app
					no_choice = train = false; break;
				case VK_SPACE: //=>discard image
					no_choice = false; break;
				case (int) 'n' : case (int)'N': //read name and use image
					cout << "Enter Name: ";
					getline(cin, curName);
					if(curName.empty()) { no_choice = true; break; }
					if(create_dir(output_dir + '/' + curName) == false){
						no_choice = true;
						cout << "Unable to create directory " << output_dir << '/' << curName << endl;
						break;
					}
				case VK_RETURN: //enter => use image
					tmp = output_dir + '/' + curName + '/' + curName + '_' + std::to_string(names[curName]++) + ".png";
					try{
						cout << "Saving " << tmp << (imwrite(tmp,resized_face,compression_params) ? " done!" : " FAILED!") << endl;
						tmp += ';' + curName + '\n';
						datasetlist << tmp;
					}catch(cv::Exception &e){
						cout << e.what() << endl;
					}
					no_choice = false; break;
				default:
					no_choice = true; break;
				}
			}
		}
	}

	datasetlist.flush();
	datasetlist.close();
	cam.release();

    return EXIT_SUCCESS;
}

