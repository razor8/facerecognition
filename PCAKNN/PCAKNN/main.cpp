//The MIT License(MIT)
//
//Copyright(c) 2014 Fabian Wahlster - razor@singul4rity.com
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions :
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#include "PCAKNN.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <Windows.h>
#include <random>
#include <time.h>

bool FileExists(LPCTSTR szPath){
	DWORD dwAttrib = GetFileAttributes(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES && !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

int main(int argc, const char *argv[]) {

	printf("WebCam-Usage: dataset.txt cascade-file.xml webcam-devID k\n",argc);
	printf("Image-Usage: dataset.txt target.png k\n",argc);

	if(argc < 4){
		return EXIT_FAILURE;
	}

	ifstream datasetlist(argv[1],ios::in);
	if(datasetlist.is_open() == false){
		printf("Unable to open datasetlist %s\n",argv[1]);
		return EXIT_FAILURE;
	}

	//parse list
	string dataline,imagefile;
	vector<Face> faces;
	int count = 0;
	//parse list and load training images
	while(datasetlist.good() && getline(datasetlist,dataline)){
		stringstream ss(dataline);
		getline(ss,imagefile,';');
		if(imagefile.empty()) continue;
		string label;ss >> label;			
		printf("Loading %s\n",imagefile.c_str());
		Mat img = imread(imagefile,0); //read grayscale image
		if(img.data == NULL) continue; //failed to load image
		Face f; f.name = label; f.face = img;
		faces.push_back(f);		
	}

	datasetlist.close();

	if(faces.size() < 1){
		printf("No Faces loaded!\n");
		return EXIT_FAILURE;
	}

	unsigned int k = 5;

	try{
		//PCA training
		PCAKNN pcaknn;
		list<Interval> intervals; intervals.push_back(Interval(faces.begin(),faces.end()));
		pcaknn.train(intervals);

		Size dim = Size(pcaknn.getWidth(), pcaknn.getHeight());

		if(argc == 5){
			int deviceId = atoi(argv[3]);
			k = (unsigned int) atoi(argv[4]);

			//start webcam
			VideoCapture cam(deviceId);
			if(cam.isOpened() == false) {
				printf("Capture Device ID %d cannot be opened\n");
				return EXIT_FAILURE;
			}

			CascadeClassifier haar_cascade;
			string cascade_file = string(argv[2]);
			//load classifier
			haar_cascade.load(cascade_file);

			Mat frame,copy,grayscale,face,resized_face,sample,target;
			vector< Rect_<int> > rects;
			bool capture = true;
			while(capture){
				//capture frame and convert to grayscale
				cam >> frame;
				copy = frame.clone();
				cvtColor(copy,grayscale,CV_BGR2GRAY);
				rects.clear();
				haar_cascade.detectMultiScale(grayscale,rects);
				printf("%d Faces detected\n",rects.size());
				for(unsigned int i = 0; i<rects.size();++i){
					face = grayscale(rects[i]);//extract face and resize
					cv::resize(face, resized_face, dim, 1.0, 1.0, INTER_CUBIC);

					//Recognition with KNN
					string name = pcaknn.recognize(resized_face,k,false/*k % 2 == 0*/);// use weighting if k is even to prevent voting ties

					cout << "Face recognized: " << name << endl;	
				}
				if(GetAsyncKeyState (VK_ESCAPE) != 0){
					capture = false;
				}
			}
			cam.release();
		}else{
			//load file from comandline
			string inputfile(argv[2]);
			k = (unsigned int)atoi(argv[3]);
			Mat target = imread(inputfile,0);
			if(target.data == NULL){
				printf("Unable to load file %s\n",inputfile.c_str());
				return EXIT_FAILURE;
			}

			if(target.cols != dim.width || target.rows != dim.height){
				printf("Expected resolution %dx%d got %dx%d\n",dim.width ,dim.height,target.cols,target.rows);
				return EXIT_FAILURE;
			}
			//run KNN
			string name = pcaknn.recognize(target,k,k % 2 == 0);

			cout << "Face recognized: " << name << endl;		
		}
	}catch(const cv::Exception &e){
		printf("%s\n",e.what());
		return EXIT_FAILURE;
	}

	//k-fold CrossValidation

	unsigned int num = k;//use same k as before
	unsigned int interval = faces.size() / num;

	printf("%d-fold CrossValidation:\n",num);

	srand(time(NULL));

	int n = faces.size();
	//permutation
	for (int i=n-1; i > 0; --i) {
		swap(faces[i],faces[rand() % (i+1)]);
	}

	list<Interval> intervals;

	//separate faces into intervals
	for(unsigned int i = 0; i<num-1;++i){
		intervals.push_back(Interval(faces.begin() + i*interval,faces.begin() + (i+1)*(interval)));
	}

	intervals.push_back(Interval(faces.begin() + (num-1)*interval,faces.end())); // overlap included	

	string name;float error = 0.f;
	for(unsigned int i = 0; i<num ;++i){
		//use last interval for cross validation
		Interval validate = intervals.back();intervals.pop_back();

		PCAKNN pk;	
		pk.train(intervals,false); unsigned int c = 0;
		for(vector<Face>::iterator itr = validate.start; itr != validate.end; itr++){
			name = pk.recognize(itr->face,k,false);
			printf("Recognized: %s Correct: %s\n\n",name.c_str(),itr->name.c_str());
			if(name.compare(itr->name) == 0) ++c;//count matches
		}
		printf("\nTest %d/%d: Correct %d/%d\n\n",i+1,num,c,validate.Length());
		error += (float)c/(float)(validate.Length());

		intervals.push_front(validate);//put cv interval back 
	}

	error /= (float)num;

	printf("Correct %f Percent\n",error * 100.f);

	return EXIT_SUCCESS;
}