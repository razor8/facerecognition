//The MIT License(MIT)
//
//Copyright(c) 2014 Fabian Wahlster - razor@singul4rity.com
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions :
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#ifndef PCAKNN_H
#define PCAKNN_H

#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <list>
#include <vector>

using namespace cv;
using namespace std;

struct Face{
	Mat face;
	string name;
};

struct Weighting{
	unsigned int count;
	double total_dist;
};

struct Interval{
	vector<Face>::iterator start, end;//end is NOT in the INTERVAL!
	Interval(vector<Face>::iterator s, vector<Face>::iterator e){//make ref?
		start = s; end = e;
	}
	int Length() const{
		return end-start;
	}
};

class PCAKNN{
private:
	Mat w,ev,mean;
	vector<Face> projections;
	unsigned int data_type;

	unsigned int width;
	unsigned int height;
	unsigned int n;
public:
	PCAKNN(unsigned int data_type_ = CV_32F);
	~PCAKNN(void);

	unsigned int getWidth() const {return width;}
	unsigned int getHeight() const {return height;}

	void train(const list<Interval> &intervals,const bool console_output = true);
	string recognize(const Mat &face, const unsigned int k = 5, const bool use_distance_weighting = true, const bool console_output = true) const;
};
#endif
