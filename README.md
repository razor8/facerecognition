PCAKNN features principal component analysis (eigenfaces), k-Nearest Neighbours and k-Fold CV implementations.
FaceDetection allows you to create a dataset (using your webcam) to use with the PCAKNN project for face recognition.

Compilation: Change the path to your OpenCV directory (Additional Library Directories too). D:\OpenCV is default.

PCAKNN parameters:

k = 7 => 7-Nearest Neighbours & 7-Fold CrossValidation

Webcam usage: <path to dataset file> <path to cascade file> <webcam device id> <k value>
Example: dataset.txt haarcascade_frontalface_default.xml 0 7

Image usage: <path to dataset file> <path to target image>
Example: dataset.txt target.png

FaceDetection parameters:

<output directory> <path to dataset file> <path to cascade file> <webcam device id> <width> <height>
Example: faces dataset.txt haarcascade_frontalface_default.xml 0 200 300

Visit: http://singul4rity.com/